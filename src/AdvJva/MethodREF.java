package AdvJva;

import java.util.*;

class Person {

	private String name;
	private Integer age;

	public Person(String name, Integer age)
	{
		this.name = name;
		this.age = age;
	}

	public Integer getAge() 
	{
		return age; 
	}
	
	public String getName()
	{
		return name;
	}
}


public class MethodREF {

	public static int compareByName(Person a, Person b)
	{
		return a.getName().compareTo(b.getName());
	}

	public static int compareByAge(Person a, Person b)
	{
		return a.getAge().compareTo(b.getAge());
	}

	public static void main(String[] args)
	{

		List<Person> persons = new ArrayList<>();

		persons.add(new Person("Ammiel", 21));
		persons.add(new Person("Roshan", 20));
		persons.add(new Person("Akshay", 24));


		Collections.sort(persons, MethodREF::compareByName);

		System.out.println("Sort by name :");

		persons.stream().map(p -> p.getName()).forEach(System.out::println);

		
		Collections.sort(persons, MethodREF::compareByAge);

		System.out.println("\nSort by age :");

		persons.stream().map(x -> x.getName()).forEach(System.out::println);
	}

}
