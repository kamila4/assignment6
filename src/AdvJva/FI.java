package AdvJva;


@FunctionalInterface

interface Calci{
	void cube(int i);
	
	default void square(int i) {
		System.out.println(i*i);
	}
	
	default void add(int i,int j) {
		
		System.out.println(i+j);
	}
	
	static void sub(int i,int j) {
		System.out.println(i-j);
	}
	
	static void mul(int i,int j) {
		
		System.out.println(i*j);
	}
	
	static void div(int i , int j) {
		System.out.println(i/j);
	}
}


public class FI implements Calci {
	
	@Override
	public void cube(int i) {
		System.out.println(i*i*i);
	}
	
	
	public static void main(String[] args) {
		FI obj = new FI();
		
		obj.add(1, 2);
		obj.cube(5);
		obj.square(5);
		Calci.div(10, 5);
		Calci.mul(14, 3);
		Calci.sub(15, 3);
		
	}

}
